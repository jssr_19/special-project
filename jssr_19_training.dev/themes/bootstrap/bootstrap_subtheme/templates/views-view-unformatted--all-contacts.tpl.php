<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?> style="width:99%; background:linear-gradient(to right bottom,dodgerblue,white 10%,whitesmoke); box-shadow:5px 5px 5px 4px gray; border:1px ridge;">
    <?php print $row;  
	?>
  </div><br>
<?php endforeach; ?>